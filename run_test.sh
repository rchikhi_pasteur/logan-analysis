#!/bin/bash
set -e
echo s3://logan-testing-march2024/c/DRR000001/DRR000001.contigs.fa.zst >  sets/test-array.txt
echo s3://logan-testing-march2024/c/DRR000002/DRR000002.contigs.fa.zst >> sets/test-array.txt
echo s3://logan-testing-march2024/c/DRR000003/DRR000003.contigs.fa.zst >> sets/test-array.txt
echo s3://logan-testing-march2024/c/DRR000005/DRR000005.contigs.fa.zst >> sets/test-array.txt
echo s3://logan-testing-march2024/c/DRR030840/DRR030840.contigs.fa.zst >> sets/test-array.txt # a meatier set that has diamond hits in july1 analysis
echo s3://logan-testing-march2024/c/ERR11280817/ERR11280817.contigs.fa.zst >> sets/test-array.txt # one that runs out of memory for yacht
#echo s3://logan-testing-march2024/c/SRR9003421/SRR9003421.contigs.fa.zst >> sets/test-array.txt # a much meatier set, 4 GB contigs
#echo s3://logan-testing-march2024/c/SRR2032467/SRR2032467.contigs.fa.zst >> sets/test-array.txt # testing potential bug

mv set set.bak && echo "test-array" > set

bash -c "cd batch && bash deploy-docker.sh"

bash process_array.sh serratus-rayan 2

rm -f sets/test-array && mv set.bak set
