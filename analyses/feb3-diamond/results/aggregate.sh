#!/bin/bash

PREFIX="meta."
OUTPUT_DIR="grep_results_$PREFIX"

# Create output directory if not exists
mkdir -p "$OUTPUT_DIR"

# Export variables so they can be used inside parallel
export OUTPUT_DIR PREFIX

rm -rf "$OUTPUT_DIR"/*

# Process each subfolder in parallel
#find /rs/data/ -name "DRR0*" -type d | parallel -j 60 --no-notice '
find /rs/data/ -mindepth 1 -maxdepth 1 -type d | parallel -j 60 --no-notice '
    subdir_name=$(basename {})
    output_file="$OUTPUT_DIR/${subdir_name}_grep.txt"
    
    # Process each file separately inside the folder
    find "{}" -type f | while read -r file; do
        awk -v prefix="$PREFIX" '"'"'$6 ~ ("^" prefix) {print}'"'"' "$file" >> "$output_file"
    done

    zstd --rm "$output_file"
'

echo "Processing completed. Results are stored in $OUTPUT_DIR."

