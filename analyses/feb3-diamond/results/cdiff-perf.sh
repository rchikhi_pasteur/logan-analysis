zstdcat grep_results_endo./*.zst | perl -ne '
BEGIN {
    open(F, "cdiff-perf.txt") or die "Cannot open cdiff-perf.txt: $!";
    while (<F>) { chomp; s/_$//; $acc{$_} = 1 }  # Remove trailing underscore when storing keys
    close F;
}
if (/^(\S+?)_/) {  # Match up to the first underscore
    print if exists $acc{$1};
}' |zstd -c > cdiff-cperf.results.txt.zst
