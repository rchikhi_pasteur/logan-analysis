#!/bin/bash

filename="cdiff-cperf.results.txt.zst"
output="cdiff-cperf.results.full_length.txt.zst"

filename="all_meta.diamond.txt.zst"
output="all_meta.diamond.full_length.txt.zst"

filename="all_mmtv.diamond.txt.zst"
output="all_mmtv.diamond.full_length.txt.zst"

zstdcat $filename | \
  awk '{
    # Get subject start and end
    start = $7; 
    end = $8;
    # Ensure start is the smaller coordinate
    if (start > end) { tmp = start; start = end; end = tmp; }
    
    # Calculate aligned length on subject
    aligned = end - start + 1;
    
    # Check if aligned length covers at least 90% of the subject length (column 9)
    if (aligned / $9 >= 0.9)
        print $0;
}' |zstd -c > $output

