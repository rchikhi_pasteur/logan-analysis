#/bin/bash
set -e
prefix=feb6
rm -f $prefix.faa
cat *.faa > tmp
mv tmp $prefix.faa
diamond makedb -d $prefix --in $prefix.faa
aws s3 cp $prefix.dmnd s3://serratus-rayan/beetles/ --acl public-read
