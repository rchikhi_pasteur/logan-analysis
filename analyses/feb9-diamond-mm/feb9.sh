#/bin/bash
set -e
prefix=feb9
rm -f $prefix.faa
cat *.faa > tmp
mv tmp $prefix.faa
diamond makedb -d $prefix --in $prefix.faa
aws s3 cp $prefix.dmnd s3://serratus-rayan/beetles/ --acl public-read

rm -f $prefix.fna
cat chris/*.fas > tmp
mv tmp $prefix.fna
sed "s/>/>chris./g" -i feb9.fna 
aws s3 cp $prefix.fna s3://serratus-rayan/beetles/ --acl public-read
