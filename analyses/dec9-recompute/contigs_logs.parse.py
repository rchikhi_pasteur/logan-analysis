import re
import sys
from collections import defaultdict

# Regex patterns to extract information
accession_pattern = re.compile(r"Downloading accession ([^\)]+)")
nt_assembled_pattern = re.compile(r"nt_assembled\s+:\s+(\d+)")
tips_removed_pattern = re.compile(r"tips removed\s+:\s+([\d+\s+\+]+)")
bulges_removed_pattern = re.compile(r"bulges removed\s+:\s+([\d+\s+\+]+)")
ec_removed_pattern = re.compile(r"EC removed\s+:\s+([\d+\s+\+]+)")
minia_start_pattern = re.compile(r"Minia 3")
done_pattern = re.compile(r"Done with (SRR\d+)")

# Helper function to sum numbers from a + separated string
def sum_numbers(s):
    return sum(map(int, re.findall(r"\d+", s)))

# Parse the log file
def parse_log():
    data = defaultdict(lambda: {
        "minia_no_redo": {},
        "minia_with_redo": {}
    })

    current_accession = None
    current_minia = None
    nb_minia3_seen = 0

    for line in sys.stdin:
        # Detect accession
        acc_match = accession_pattern.search(line)
        if acc_match:
            current_accession = acc_match.group(1)
            current_minia = "minia_no_redo"
            nb_minia3_seen = 0
            continue

        # Detect nt_assembled
        if current_accession:
            nt_assembled_match = nt_assembled_pattern.search(line)
            if nt_assembled_match:
                data[current_accession][current_minia]["nt_assembled"] = int(nt_assembled_match.group(1))

            # Detect tips removed
            tips_match = tips_removed_pattern.search(line)
            if tips_match:
                data[current_accession][current_minia]["tips_removed"] = sum_numbers(tips_match.group(1))

            # Detect bulges removed
            bulges_match = bulges_removed_pattern.search(line)
            if bulges_match:
                data[current_accession][current_minia]["bulges_removed"] = sum_numbers(bulges_match.group(1))

            # Detect EC removed
            ec_match = ec_removed_pattern.search(line)
            if ec_match:
                data[current_accession][current_minia]["ec_removed"] = sum_numbers(ec_match.group(1))

            # Switch to "minia_with_redo" on second occurrence of Minia 3
            if minia_start_pattern.search(line):
                nb_minia3_seen += 1
                if nb_minia3_seen == 2:
                    current_minia = "minia_with_redo"
                elif nb_minia3_seen > 2:
                    print("unexpected number of minia runs for accession",current_accession)


            # Detect completion of an accession
            done_match = done_pattern.search(line)
            if done_match:
                current_accession = None
                current_minia = None

    return data

# Main execution
if __name__ == "__main__":
    results = parse_log()

    # Print results
    for accession, metrics in results.items():
        print(f"Accession: {accession}")
        for round_name, round_metrics in metrics.items():
            print(f"  {round_name}:")
            for metric, value in round_metrics.items():
                print(f"    {metric}: {value}")
        print()

