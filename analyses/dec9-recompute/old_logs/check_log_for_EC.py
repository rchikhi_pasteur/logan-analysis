import sys
import re
from collections import defaultdict

# Function to parse the logfile
def parse_logfile():
    # Categories based on conditions
    categories = {
        "no_redo_links": "from_fixed_contigs.no_redo_links.fa.log.txt to",
        "redo_links": "from_fixed_contigs.redo_links.fa to",
        "unitigs_redo_links": "from_unitigs.redo_links.fa.log.txt to",
    }

    # Storage for categorized lines
    parsed_logs = defaultdict(list)

    current_category = None

    for line in sys.stdin:
            # Check for category change
            for category, marker in categories.items():
                if marker in line:
                    current_category = category
                    break

            # Append line to the current category if applicable
            if current_category:
                parsed_logs[current_category].append(line.strip())

    return parsed_logs

# Function to write parsed logs to separate files
def write_parsed_logs(parsed_logs):
    for category, lines in parsed_logs.items():
        output_file = f"parsed_{category}_logs.txt"
        with open(output_file, 'w') as file:
            file.write("\n".join(lines))
        print(f"Logs for '{category}' written to '{output_file}'")

# Example usage
if __name__ == "__main__":
    parsed_logs = parse_logfile()
    write_parsed_logs(parsed_logs)

