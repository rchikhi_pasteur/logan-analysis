import re
import sys

def parse_and_calculate(input_data):
    # Initialize variables
    results = []
    accession = None
    data = {}
    current_label = None

    # Parse the input line by line
    for line in input_data.splitlines():
        # Check for accession line
        accession_match = re.match(r"^Accession:\s+(\S+)", line)
        if accession_match:
            if accession and data:
                results.append((accession, data))
                data = {}
            accession = accession_match.group(1)

        # Check for label line
        label_match = re.match(r"^\s+(\S+):$", line)
        if label_match:
            current_label = label_match.group(1)
            data[current_label] = {}

        # Check for data lines
        data_match = re.match(r"^\s+(\w+_removed):\s+(\d+)", line)
        if data_match and current_label:
            key, value = data_match.groups()
            data[current_label][key] = int(value)
        data_match = re.match(r"^\s+(nt_assembled):\s+(\d+)", line)
        if data_match and current_label:
            key, value = data_match.groups()
            data[current_label][key] = int(value)


    if accession and data:
        results.append((accession, data))


    # Calculate percentages and collect data for mean computation
    ec_to_tips_values = []
    ec_to_nt_values = []

    # Calculate percentages and display
    output = []
    for accession, data in results:
        for label, stats in data.items():
            ec_removed = stats.get("ec_removed", 0)
            tips_removed = stats.get("tips_removed", 0)
            nt_assembled = stats.get("nt_assembled", 0)

            # Only compute percentages for the label itself, not individual fields
            if label in ["minia_no_redo", "minia_with_redo"]:
                ec_to_tips_percentage = (ec_removed / tips_removed) * 100 if tips_removed else 0
                ec_to_nt_percentage = (ec_removed / nt_assembled) * 100 if nt_assembled else 0

                output.append(f"Accession: {accession}, Label: {label}, %EC_to_Tips: {round(ec_to_tips_percentage, 3)}%, %EC_to_Nt: {round(ec_to_nt_percentage, 5)}%")

            # Only collect values for "minia_with_redo"
            if label == "minia_with_redo":
                ec_to_tips_values.append(ec_to_tips_percentage)
                ec_to_nt_values.append(ec_to_nt_percentage)

    # Calculate overall means
    mean_ec_to_tips = sum(ec_to_tips_values) / len(ec_to_tips_values) if ec_to_tips_values else 0
    mean_ec_to_nt = sum(ec_to_nt_values) / len(ec_to_nt_values) if ec_to_nt_values else 0

    print(f"Overall Mean %EC_to_Tips for minia_with_redo: {round(mean_ec_to_tips, 2)}%")
    print(f"Overall Mean %EC_to_Nt for minia_with_redo: {round(mean_ec_to_nt, 3)}%")

    return output

# Read input from stdin
input_data = sys.stdin.read().strip()

output = parse_and_calculate(input_data)

# Display results
for line in output:
    print(line)

