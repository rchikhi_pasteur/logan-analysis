#!/bin/bash

count () {
	suffix=$1
    date=feb12

    # so it seems that s5cmd didnt work there
    if ! aws s3 cp s3://serratus-rayan/beetles/logan_${date}_run/circles-concat/raw/complex."${suffix}".fa.zst - \
        | zstdcat \
        | awk -f count_bytes.awk \
        > completeness/complex."${suffix}".count_bytes; then
        echo "Error: s5cmd failed for complex.${suffix}.fa.zst"
        return 1
    fi

    if ! aws s3 cp s3://serratus-rayan/beetles/logan_${date}_run/circles-concat/raw/selfloops."${suffix}".fa.zst - \
        | zstdcat \
        | awk -f count_bytes.awk \
        > completeness/selfloops."${suffix}".count_bytes; then
        echo "Error: s5cmd failed for selfloops.${suffix}.fa.zst"
        return 1
    fi
}
export -f count 

ls plist.acc.txt_split -1 | parallel -j 46 count {}

