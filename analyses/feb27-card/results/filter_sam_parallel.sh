rm -f /tmp/sam_batch_*

find /rs/card -name "*.sam" | split -l 100000 -d - /tmp/sam_batch_

parallel -j64  --bar '
    batch={}
    out_file="/rs/card_filtered/batch_$(basename $batch).sam.zst"

    rm -f "$out_file"

    # Process all files in the batch sequentially
    while read samfile; do
        awk -v OFS="\t" '"'"'!/^@/ {
            s=0
            cigar=$6
            while (match(cigar, /([0-9]+)M/, m)) { 
                s += m[1]
                cigar = substr(cigar, RSTART+RLENGTH)
            }
            if (s >= 100) { $10 = "*"; print }
        }'"'"' "$samfile"
    done < "$batch" | zstd -c > "$out_file"

    echo "Batch processed: $out_file"
' ::: /tmp/sam_batch_*

rm /tmp/sam_batch_*

