#/bin/bash
set -e
prefix=feb27
rm -f $prefix.fna
cat *.fna > tmp
mv tmp $prefix.fna
aws s3 cp $prefix.fna s3://serratus-rayan/beetles/ --acl public-read
