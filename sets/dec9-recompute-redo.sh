#does a aws s3 ls faster
bash ../utils/parallel_listing.sh "s3://logan-staging/c" to-redo.listing.txt
cat to-redo.listing.txt |awk '{print $2}' |sed 's/\///' > to-redo.done.txt
rm -f to-redo.listing.txt
comm -13 to-redo.done.txt pub-c.acc.txt |sort|uniq > to-redo.acc.txt

grep -Fwf to-redo.acc.txt ~/logan-analysis/sets/pub-c.files.txt > to-redo.txt
rm -f to-redo.done.txt
echo "to-redo" > ../set
