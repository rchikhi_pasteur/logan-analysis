SELECT acc
FROM sra.metadata as s
WHERE consent = 'public' and avgspotlen >= 31
AND librarysource IN ('TRANSCRIPTOMIC', 'METATRANSCRIPTOMIC') OR 
        assay_type = 'RNA-Seq'
