#!/bin/bash

if [ "$#" -lt 2 ]; then
    echo "Usage: $0 <folder> <outfile> [--recursive]"
    exit 1
fi


s3path=$1
outfile=$2

recursive=""
if [ "$#" -eq 3 ] && [ "$3" == "--recursive" ]; then
    recursive="*"
fi

for prefix in DRR ERR SRR ;do
    for prefix2 in $(seq 0 9) ;do
        p=${prefix}${prefix2}
		if [[ "$p" == "SRR1" || "$p" == "SRR2" ]]; then
            for subprefix in $(seq 0 9); do
                subp=${p}${subprefix}
                \time s5cmd ls "$s3path/$subp$recursive" > "${outfile}_${subp}" &
            done
		else
	        \time s5cmd ls "$s3path/$p$recursive" > "${outfile}_${p}" &
        fi
    done
done
wait
cat "${outfile}_"* > "$outfile"
rm -f "${outfile}_"*
echo "Parallel listing results in $outfile"
