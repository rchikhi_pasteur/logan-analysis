# Docker Base: amazon linux 2023
FROM amazonlinux:2023

ARG PROJECT='logan-analysis'
ARG TYPE='runtime'
ARG VERSION='0.0.1'

# Additional Metadata
LABEL container.base.image="amazonlinux:2"
LABEL project.name=${PROJECT}
LABEL project.website="https://gitlab.pasteur.fr/rchikhi_pasteur/logan-analysis"
LABEL container.type=${TYPE}
LABEL container.version=${VERSION}
LABEL container.description="logan-analysis-base image"
LABEL software.license="MIT"
LABEL tags="logan"

# Update Core
RUN yum -y update
RUN yum -y install bash wget time unzip zstd \
           which sudo jq tar bzip2 grep git \
           procps  #`free` command
RUN python3 -m ensurepip

# AWS S3
ENV PIP_ROOT_USER_ACTION=ignore
RUN pip3 install boto3
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" &&\
    unzip awscliv2.zip &&\
    ./aws/install

# palmscan
RUN aws s3 cp s3://serratus-rayan/tools/palmscan2 /usr/local/bin --no-sign-request && chmod +x /usr/local/bin/palmscan2

# gotranseq is buggy, randomly crashes with deadlock
#RUN wget https://github.com/feliixx/gotranseq/releases/download/v0.4.0/gotranseq_0.4.0_Linux_x86_64.tar.gz && \
#    tar xf gotranseq_0.4.0_Linux_x86_64.tar.gz && \
#    rm gotranseq_0.4.0_Linux_x86_64.tar.gz && \
#    mv gotranseq /usr/local/bin/

# transeq
#RUN mkdir /emboss && cd /emboss && aws s3 cp s3://serratus-rayan/tools/emboss-5.0.0-h9f0ad1d_1.tar.bz2 . --no-sign-request && \
#    tar xf emboss-5.0.0-h9f0ad1d_1.tar.bz2
#ENV PATH="$PATH:/emboss/bin"

# s5cmd
RUN wget https://github.com/peak/s5cmd/releases/download/v2.2.2/s5cmd_2.2.2_Linux-64bit.tar.gz &&\
  tar xf s5cmd_2.2.2_Linux-64bit.tar.gz &&\
  rm s5cmd_2.2.2_Linux-64bit.tar.gz &&\
  mv s5cmd  /usr/local/bin

# rclone
RUN wget https://downloads.rclone.org/v1.66.0/rclone-v1.66.0-linux-amd64.zip && \
 unzip rclone-v1.66.0-linux-amd64.zip && \
 rm rclone-v1.66.0-linux-amd64.zip && \
 mv rclone-v1.66.0-linux-amd64/rclone /usr/local/bin/rclone
RUN mkdir -p /root/.config/rclone/
COPY rclone.conf /root/.config/rclone

# Robert's custom 16s search tool
RUN aws s3 cp s3://serratus-rayan/tools/usearch_16s /usr/local/bin --no-sign-request && chmod +x /usr/local/bin/usearch_16s
RUN aws s3 cp s3://serratus-rayan/tools/usearch_16s.gg97.bitvec /  --no-sign-request 

# seqtk
RUN aws s3 cp s3://serratus-rayan/tools/seqtk-1.4-he4a0461_2.tar.bz2 . --no-sign-request && \
    tar xf seqtk-1.4-he4a0461_2.tar.bz2 && mv bin/seqtk /usr/local/bin

# seqkit
RUN wget https://github.com/shenwei356/seqkit/releases/download/v2.9.0/seqkit_linux_amd64.tar.gz && \
    tar xf seqkit_linux_amd64.tar.gz && mv seqkit /usr/local/bin && cp /usr/local/bin/seqkit / 

#
# circle detection
RUN git clone https://gitlab.pasteur.fr/rchikhi_pasteur/circles-logan.git
RUN pip3 install networkx numpy

# minia
RUN aws s3 cp s3://serratus-rayan/tools/minia /usr/local/bin --no-sign-request && chmod +x /usr/local/bin/minia

# diamond
RUN wget https://github.com/bbuchfink/diamond/releases/download/v2.1.9/diamond-linux64.tar.gz && tar xf diamond-linux64.tar.gz && mv diamond /usr/local/bin

# minimap2
RUN wget https://github.com/lh3/minimap2/releases/download/v2.28/minimap2-2.28_x64-linux.tar.bz2 && tar xf minimap2-2.28_x64-linux.tar.bz2  && mv minimap2-2.28_x64-linux/minimap2 /usr/local/bin

#prodigal
#RUN aws s3 cp s3://serratus-rayan/tools/prodigal . --no-sign-request && chmod +x prodigal && mv prodigal /usr/local/bin/ 
#RUN pip3 install pyrodigal

#David Koslicki's fracminhash
# Install Miniconda
RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /tmp/miniconda.sh && \
    bash /tmp/miniconda.sh -b -p /opt/conda && \
    rm /tmp/miniconda.sh && \
    /opt/conda/bin/conda clean -ya
# Set up Conda environment, YACHT, sourmash, fmh-funprofiler
ENV PATH="/opt/conda/bin:$PATH"
#RUN conda create -y -n logan-env -c bioconda -c conda-forge yacht pandas biom-format networkx numpy && \
#RUN conda create -y -n logan-env -c bioconda -c conda-forge pandas biom-format networkx numpy && \
#    conda clean -a  && \
#   conda init && source ~/.bashrc
# install yacht through github to get latest updates
RUN yum groupinstall -y "Development Tools"
RUN git clone https://github.com/KoslickiLab/YACHT.git && cd YACHT && \
    conda env create -f env/yacht_env.yml && \
    conda init && source ~/.bashrc && conda activate yacht_env && \
    pip install .
# Install fmh-funprofiler
RUN git clone https://github.com/KoslickiLab/fmh-funprofiler.git && \
    wget --progress=dot:mega https://zenodo.org/records/10045253/files/KOs_sketched_scaled_1000.sig.zip

# logan contigs repetition fix
#RUN aws s3 cp s3://serratus-rayan/tools/fix_repeated_31mers /usr/local/bin --no-sign-request && chmod +x /usr/local/bin/fix_repeated_31mers

# Scripts from previous run
#RUN git clone https://gitlab.pasteur.fr/rchikhi_pasteur/erc-unitigs-prod
#RUN pip3 install boto3

# f2sz for compression
RUN aws s3 cp s3://serratus-rayan/tools/f2sz /usr/local/bin --no-sign-request && chmod +x /usr/local/bin/f2sz


# copy diamond DB
#RUN aws s3 cp s3://serratus-rayan/beetles/april26.dmnd . --no-sign-request
#RUN aws s3 cp s3://serratus-rayan/beetles/STB.fa . --no-sign-request
#RUN aws s3 cp s3://serratus-rayan/beetles/july1.dmnd . --no-sign-request
#RUN aws s3 cp s3://serratus-rayan/beetles/july1.dmnd.seed_idx . --no-sign-request
#RUN aws s3 cp s3://serratus-rayan/beetles/rep12.fa . --no-sign-request
#RUN aws s3 cp s3://serratus-rayan/beetles/aug24.dmnd . --no-sign-request
#RUN aws s3 cp s3://serratus-rayan/beetles/aug24.dmnd.seed_idx . --no-sign-request
#RUN aws s3 cp s3://serratus-rayan/beetles/aug26.fna . --no-sign-request
#RUN aws s3 cp s3://serratus-rayan/beetles/aug26.dmnd . --no-sign-request
#RUN aws s3 cp s3://serratus-rayan/beetles/aug26.dmnd.seed_idx . --no-sign-request
#RUN aws s3 cp s3://serratus-rayan/beetles/GCF_034140825.1_ASM3414082v1_genomic.fna.gz . --no-sign-request
#RUN aws s3 cp s3://serratus-rayan/beetles/jan7.fna . --no-sign-request
#RUN aws s3 cp s3://serratus-rayan/beetles/feb3.dmnd . --no-sign-request
#RUN aws s3 cp s3://serratus-rayan/beetles/feb3.dmnd.seed_idx . --no-sign-request
#RUN aws s3 cp s3://serratus-rayan/beetles/feb6.dmnd . --no-sign-request
#RUN aws s3 cp s3://serratus-rayan/beetles/feb9.dmnd . --no-sign-request
#RUN aws s3 cp s3://serratus-rayan/beetles/feb9.fna . --no-sign-request
RUN aws s3 cp s3://serratus-rayan/beetles/feb27.fna . --no-sign-request

# Copy Scripts
COPY logan-analysis.sh /
RUN mkdir /tasks
COPY tasks/* /tasks/

# (for local testing ; will be already mounted in Batch production)
RUN mkdir -p /localdisk

# Increase the default chunksize for `aws s3 cp`.  By default it is 8MB,
# which results in a very high number of PUT and POST requests.  These
# numbers have NOT been experimented on, but chosen to be just below the
# max size for a single-part upload (5GB).  I haven't pushed it higher
# because I don't want to test the edge cases where a filesize is around
# the part limit.
# Configure AWS Locally
RUN chmod +x logan-analysis.sh  \
 && aws configure set default.region us-east-1 \
 && aws configure set default.s3.multipart_threshold 4GB \
 && aws configure set default.s3.multipart_chunksize 4GB \
 && echo "Dockerfile done"
#==========================================================
# ENTRYPOINT ==============================================
#==========================================================
ENTRYPOINT ["./logan-analysis.sh"]
