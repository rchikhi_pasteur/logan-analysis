#!/bin/bash
## copy redone, for the v1.1 contigs in jan 2025

set -eu

# Initialize variables to hold the last executed command and its line number.
LAST_CMD=""
LAST_CMD_LINE=0

# Function to log the last command executed (for DEBUG trap).
log_last_command() {
    LAST_CMD="$BASH_COMMAND"
    LAST_CMD_LINE="$BASH_LINENO"
}

handle_error() {
    local error_code=$?
    echo "Error occurred in COPY function at line $LAST_CMD_LINE: '$LAST_CMD' exited with status $error_code."
}

cleanup() {
    if [[ -n "$accession" ]]; then
        rm -Rf /localdisk/"$accession"
    fi
    echo "Cleanup of $accession complete."
}

task() {
	trap 'log_last_command' DEBUG
    trap 'handle_error $LINENO' ERR
    trap 'cleanup' EXIT
	
    s3file=$1
	THREADS=$2
	outbucket=$3

    echo "COPY task for file: $s3file"
    echo "outbucket: $outbucket"
    filename=$(echo $s3file | awk -F/ '{print $NF}')
    accession=$(echo $filename | cut -d '.' -f1)
    filename_noz=${filename%.*}
    filename_trans=$filename_noz.transeq

    # Check if this job wasn't already done
    if s5cmd ls "s3://logan-pub/c1.0/$accession/$filename" >/dev/null 2>&1; then
        if s5cmd ls "s3://logan-pub/c/$accession/$filename" >/dev/null 2>&1; then
            echo "accession $accession was partially done, continuing without overwriting c1.0"
        else
            echo "accession $accession has c1.0 but not c, continuing without overwriting c1.0"
            #echo "accession $accession already done (probably in the pilot)"
            #return 0
        fi
    else
        # for moving the v1.0 contigs: don't even download locally, do a server-side copy from s3://logan-pub/c/$acc/ to s3://logan-pub/c1.0/$acc/
        clean_path="${s3file#s3://}"
        if ! \time rclone move aws:$clean_path aws:"$outbucket"/c1.0/"$accession"/ --s3-use-already-exists 0 --s3-no-check-bucket -v; then
            return 1  # Trigger error handling if s3 cp fails.
        fi
    fi 

    mkdir -p /localdisk/"$accession"
    cd /localdisk/"$accession" || exit

    # opportunistically run palmscan and 16s analysis on contigs
    if [[ "$filename" == *"contigs"* ]]; then
        folder="c"
        echo "Downloading accession $accession"
        if ! \time s5cmd cp -c $THREADS s3://logan-staging/c/"$accession"/"$filename" "$filename" ; then
            return 1  # This ensures the error trap is triggered if s3 cp fails.
        fi

        # decompress and drop short contigs
        \time zstd -d -c $filename |seqtk seq -L 200 > $filename_noz

        # palmscan2 analysis
        # needs 4 cores! 2 sometimes it crashes due to lack of mem, likely
        [ -s $filename_noz ] && \time palmscan2 -fasta_xlat $filename_noz -fastaout $filename_trans -threads $THREADS
        [ -s $filename_noz ] && \time palmscan2 -search_pssms $filename_trans -tsv "$filename_trans".hits.tsv -min_palm_score 5.0 -fasta "$filename_trans".pps.fa -threads $THREADS
        [ -s "$filename_trans".hits.tsv ] && s5cmd cp -c 1 "$filename_trans".hits.tsv s3://serratus-rayan/logan_palmscan_contigs_v1.1/"$accession"/
        [ -s "$filename_trans".pps.fa   ] && s5cmd cp -c 1 "$filename_trans".pps.fa   s3://serratus-rayan/logan_palmscan_contigs_v1.1/"$accession"/

        # 16s analysis
        [ -s $filename_noz ] && usearch_16s \
          -search_16s $filename_noz \
          -bitvec /usearch_16s.gg97.bitvec \
          -fastaout "$filename_noz".16s.fa \
          -tabbedout "$filename_noz".16s_results.txt \
          -threads $THREADS
        [ -s "$filename_noz".16s.fa ]               && s5cmd cp -c 1 "$filename_noz".16s.fa          s3://serratus-rayan/logan_16s_contigs_v1.1/"$accession"/
        [ -s "$filename_noz".16s_results.txt ]      && grep -v "wins=0\sgenes=0\sfrags=0" "$filename_noz".16s_results.txt > "$filename_noz".16s_results.filt.txt || true
        [ -s "$filename_noz".16s_results.filt.txt ] && s5cmd cp -c 1 "$filename_noz".16s_results.filt.txt s3://serratus-rayan/logan_16s_contigs_v1.1/"$accession"/

		# Check if file already exists in S3:
		if s5cmd ls "s3://$outbucket/$folder/$accession/$filename" >/dev/null 2>&1; then
			echo "Object exists in S3; skipping upload to avoid overwrite."
		else
			echo "Uploading accession $accession"
			if ! \time s5cmd cp -c $THREADS "$filename" s3://"$outbucket"/"$folder"/"$accession"/; then
				return 1  # Trigger error handling if s3 cp fails.
			fi
        fi
    else
        echo "Not a contig?! $filename"
        return 1
    fi

    rm -Rf /localdisk/"$accession"
    echo "Done with $accession"
    trap '' EXIT
}

export -f task
