#!/bin/bash

set -eu

# Initialize variables to hold the last executed command and its line number.
LAST_CMD=""
LAST_CMD_LINE=0

# Function to log the last command executed (for DEBUG trap).
log_last_command() {
    LAST_CMD="$BASH_COMMAND"
    LAST_CMD_LINE="$BASH_LINENO"
}

handle_error() {
    local error_code=$?
    echo "Error occurred in COPY function at line $LAST_CMD_LINE: '$LAST_CMD' exited with status $error_code."
}

cleanup() {
    if [[ -n "$accession" ]]; then
        rm -Rf /localdisk/"$accession"
    fi
    echo "Cleanup of $accession complete."
}

task() {
	trap 'log_last_command' DEBUG
    trap 'handle_error $LINENO' ERR
    trap 'cleanup' EXIT
	
	s3file=$1
    THREADS=$2
    # disregards the third argument (output bucket), we're hardcoding output paths here

    echo "Logan analysis ('recompute', december 2024) task for file: $s3file"
    filename=$(echo $s3file | awk -F/ '{print $NF}')
    accession=$(echo $filename | cut -d '.' -f1)
    filename_noz=${filename%.*}

    mkdir -p /localdisk/"$accession"
    cd /localdisk/"$accession" || exit
   
	echo "Downloading accession $accession"
	if ! \time s5cmd cp -c $THREADS "$s3file" "$filename" ; then
		echo "Error with s5cmd cp, function error should trap. If not, cleaning up anyway. Remove this if all OK"
	    rm -Rf /localdisk/"$accession"
		return 1  # This ensures the error trap is triggered if s3 cp fails. TODO check this
	fi
   
    if ! zstd --test $filename ; then
        touch $accession.corrupt
        s5cmd cp $accession.corrupt s3://serratus-rayan/logan_corrupt_contigs/
        # here we want to just ignore that accession and continue
        echo "Corrupt accession!"
	    rm -Rf /localdisk/"$accession"
		return 0 
	fi

	# Get the file size in bytes
	file_size=$(stat -c %s "$filename")
    empty_accession=0

	# decompress
    \time zstd -d -c $filename > $filename_noz
    rm -f $filename # saves space



    # CHANGEME
    outdate=dec9

    s3dest=s3://serratus-rayan/beetles/logan_${outdate}_run/contigs/$accession/
    s3statsdest=s3://serratus-rayan/beetles/logan_${outdate}_run/stats/$accession/

    # fix circles
    filename_fixed=${filename_noz%.*}.fixed.fa
    \time fix_repeated_31mers  $filename_noz $filename_fixed
    
    # minia from unitigs, from scratch
    if false;
    then
    aws s3 cp s3://logan-pub/u/$accession/$accession.unitigs.fa.zst .
    zstd -d $accession.unitigs.fa.zst
    echo -e ">1\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" > $accession
    perl -i -pe "if (/^>/) { s/^>${accession}_/>/; s/ka:/km:/; }" "${accession}.unitigs.fa"
    /usr/bin/time minia -in $accession -nb-cores $THREADS -max-memory 1 -skip-bcalm -skip-bglue -redo-links 2>log.txt
    perl -i -pe "if (/^>/) { s/^>/>${accession}_/; s/km:/ka:/; s/LN:i:\S+ //; s/KC:i:\S+ // }" "${accession}.contigs.fa"
    outfile="${accession}.contigs.from_unitigs.redo_links.fa"
    mv "${accession}.contigs.fa" $outfile
	aws s3 cp $outfile $s3dest
    echo "$outfile" > $outfile.log.txt
    tail -n 3 log.txt >> $outfile.log.txt
    rm -f log.txt
    seqkit stats -a $outfile >> $outfile.log.txt
    aws s3 cp "$outfile.log.txt" $s3statsdest
    fi
    
    # minia from contigs, without redo links
    if false;
    then
    cp -f $filename_fixed "${accession}.unitigs.fa"
    rm -f ${accession}.h5
    echo -e ">1\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" > $accession
    perl -i -pe "if (/^>/) { s/^>${accession}_/>/; s/ka:/km:/; }" "${accession}.unitigs.fa"
    /usr/bin/time minia -in $accession -nb-cores $THREADS -max-memory 1 -skip-bcalm -skip-bglue -skip-links 2>log.txt
    perl -i -pe "if (/^>/) { s/^>/>${accession}_/; s/km:/ka:/; s/LN:i:\S+ //; s/KC:i:\S+ // }" "${accession}.contigs.fa"
    outfile="${accession}.contigs.from_fixed_contigs.no_redo_links.fa"
    mv "${accession}.contigs.fa" $outfile
	aws s3 cp $outfile $s3dest
    echo "$outfile" > $outfile.log.txt
    tail -n 3 log.txt >> $outfile.log.txt
    rm -f log.txt
    seqkit stats -a $outfile >> $outfile.log.txt
    aws s3 cp "$outfile.log.txt" $s3statsdest
    fi 
    
    # minia from contigs, redo links
    # added no ec removal
    cp $filename_fixed "${accession}.unitigs.fa"
    rm -f ${accession}.h5
    echo -e ">1\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" > $accession
    perl -i -pe "if (/^>/) { s/^>${accession}_/>/; s/^>_/>/; s/ka:/km:/; }" "${accession}.unitigs.fa"
    /usr/bin/time minia -in $accession -nb-cores $THREADS -max-memory 1 -skip-bcalm -skip-bglue -redo-links -no-ec-removal 2>log.txt
    perl -i -pe "if (/^>/) { s/^>/>${accession}_/; s/km:/ka:/; s/LN:i:\S+ //; s/KC:i:\S+ // }" "${accession}.contigs.fa"
    seqkit stats -a $accession.contigs.fa >> $accession.contigs.fa.stats.txt


    # post processing
    f2sz -l 12 \
                        -b 128M \
                        -F \
                        -i \
                        -f \
                        -T $THREADS\
			"${accession}.contigs.fa"

    size_before=$(stat --printf="%s" ${accession}.contigs.fa)
    size_after=$(stat --printf="%s" ${accession}.contigs.fa.zst)

	# record stats to dynamodb
	python3 -c "
import sys
sys.path.append('/erc-unitigs-prod/batch/src')
from utils import ddb_log 
ddb_log('$accession', 'size_contigs_before_compression', '$size_before')
ddb_log('$accession','size_contigs_after_compression', '$size_after')
"

	aws s3 cp ${accession}.contigs.fa.zst s3://logan-staging/c/${accession}/${accession}.contigs.fa.zst

	# record stats to dynamodb
	python3 -c "
import sys
sys.path.append('/erc-unitigs-prod/batch/src')
from seqstats import seqstats
ret, nbseq = seqstats('$accession', 'contigs')
print(f'Result: {ret}, Number of Sequences: {nbseq}')
	"

	rm -Rf /localdisk/"$accession"
	echo "Done with $accession"
	trap '' EXIT
}

export -f task
