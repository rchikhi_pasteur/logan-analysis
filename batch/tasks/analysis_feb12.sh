#!/bin/bash

THREADS=4

set -eu

# Initialize variables to hold the last executed command and its line number.
LAST_CMD=""
LAST_CMD_LINE=0

# Function to log the last command executed (for DEBUG trap).
log_last_command() {
    LAST_CMD="$BASH_COMMAND"
    LAST_CMD_LINE="$BASH_LINENO"
}

handle_error() {
    local error_code=$?
    echo "Error occurred in function at line $LAST_CMD_LINE: '$LAST_CMD' exited with status $error_code."
}

cleanup() {
    if [[ -n "$accession" ]]; then
        rm -Rf /localdisk/"$accession"
    fi
    echo "Cleanup of $accession complete."
}

task() {
	trap 'log_last_command' DEBUG
    trap 'handle_error $LINENO' ERR
    trap 'cleanup' EXIT
	
	s3file=$1
    # disregards the second argument (output bucket), we're hardcoding output paths here

    echo "Logan Analysis ('circles1.1', feb 2025) task for file: $s3file"
    filename=$(echo $s3file | awk -F/ '{print $NF}')
    accession=$(echo $filename | cut -d '.' -f1)
    filename_noz=${filename%.*}

    mkdir -p /localdisk/"$accession"
    cd /localdisk/"$accession" || exit
   
	echo "Downloading accession $accession"
	if ! \time s5cmd cp -c $THREADS "$s3file" "$filename" ; then
		return 1  # This ensures the error trap is triggered if s3 cp fails.
	fi
   
    if ! zstd --test $filename ; then
        touch $accession.corrupt
        s5cmd cp $accession.corrupt s3://serratus-rayan/logan_corrupt_contigs/
        # here we want to just ignore that accession and continue
	    rm -Rf /localdisk/"$accession"
		return 0 
	fi

	# Get the file size in bytes
	file_size=$(stat -c %s "$filename")
    empty_accession=0

	# Check if the file size is less than 200 bytes
	#if [ "$file_size" -lt 200 ]; then
	#	echo "Contigs file is smaller than 200 bytes. Zstd somehow hangs on small files, so, skipping it entirely."
    #    empty_accession=1
    #else
    if [ "$file_size" -lt 200 ]; then
        empty_accession=1
        touch $filename_noz.circles.fa 
    fi
	    # still decompress
    	\time zstd -d $filename
    #fi
    rm -f $filename
 
    #
    # activate the environment
    conda init
    source ~/.bashrc
    conda activate yacht_env

  
    if false;
    then
    # circles
    circles_status=1
    [ -s $filename_noz ] && {
    \time python3 /circles-logan/circles.py $filename_noz 31 $filename_noz.circles.fa 
    circles_status=$?
    } || true
    [[ $circles_status -eq 0 ]] && touch $filename_noz.circles.fa 
    [ -f $filename_noz.circles.fa ] && s5cmd cp -c 1 "$filename_noz".circles.fa s3://serratus-rayan/beetles/logan_feb12_run/circles/$accession/
    fi

    # Koslicki analysis

    if true;
    then
            # get unitigs
            if ! \time s5cmd cp -c $THREADS "s3://logan-pub/u/$accession/$accession.unitigs.fa.zst" . ; then
                return 1  # This ensures the error trap is triggered if s3 cp fails.
            fi
            zstd -d $accession.unitigs.fa.zst
            rm -f $accession.unitigs.fa.zst

            file_size=$(stat -c %s "$accession.unitigs.fa")
            empty_accession=0
            if [ "$file_size" -lt 200 ]; then
                empty_accession=1
                touch $accession.empty 
                s5cmd cp -c 1 $accession.empty s3://serratus-rayan/beetles/logan_feb12_run/yacht/$accession/
                rm -Rf /localdisk/"$accession"
                echo "uploading empty yacht results because empty accession"
                return 0
            fi
      
            # Sketch the sample
            echo "Running yacht sketch sample for accession $accession"
            #sourmash sketch dna -f -p k=15,k=31,k=33,k=51,scaled=1000,abund -o ${accession}.unitigs.fa.sig.zip ${accession}.unitigs.fa
            #yacht sketch sample --infile ${accession}.unitigs.fa --kmer 31 --scaled 1000 --outfile ${accession}.unitigs.fa.sig.zip
            export RAYON_NUM_THREADS=1
            printf "name,genome_filename,protein_filename\n${accession},/localdisk/${accession}/${accession}.unitigs.fa," > manysketch.csv
            sourmash scripts manysketch -o /localdisk/${accession}/${accession}.unitigs.fa.sig.zip -p dna,k=15,k=31,k=33,scaled=1000,noabund -c 1 -f manysketch.csv

            {
            # Run yacht analysis, ANI 0.95
            echo "Running yacht analysis for accession $accession"
            echo "I'm in `pwd`"
            /usr/bin/time -v yacht run --json /gtdb-rs214-reps.k31_0.95_pretrained/gtdb-rs214-reps.k31_0.95_config.json \
                --sample_file ${accession}.unitigs.fa.sig.zip --significance 0.99 --num_threads $THREADS \
                --min_coverage_list 1 0.5 0.25 0.125 0.0625 0.03125 0.015625 0 --out ./result_0.95_ANI_${accession}.xlsx

            if [ $? -eq 0 ]; then
                # Run functional profiling
                echo "Running functional profiling for accession $accession"
                /usr/bin/time -v python /fmh-funprofiler/funcprofiler.py ${accession}.unitigs.fa /KOs_sketched_scaled_1000.sig.zip 11 1000 ${accession}_functional_profile
            fi
            } || true

            upload_dir=/localdisk/$accession/2upload/
            mkdir -p $upload_dir

            # Uploading results to S3
            if [[ -f "${accession}.unitigs.fa.sig.zip" && -f "${accession}_functional_profile" ]]; then
                echo "Uploading results for $accession"
                 mkdir -p ${upload_dir}/taxa_profiles ${upload_dir}/sigs_dna ${upload_dir}/sigs_aa ${upload_dir}/func_profiles
                 if [[ -f result_0.95_ANI_${accession}.xlsx ]]; then
                     cp "/localdisk/${accession}/result_0.95_ANI_${accession}.xlsx" ${upload_dir}/taxa_profiles/
                 fi
                 cp "/localdisk/${accession}/${accession}.unitigs.fa.sig.zip" ${upload_dir}/sigs_dna/
                 cp "/localdisk/${accession}/${accession}_functional_profile" ${upload_dir}/func_profiles/
                 # The following have timestamps on them, so need to look for the file names
                 for file in /localdisk/${accession}/${accession}.unitigs.fa_sketch_*; do
                    if [ -e "$file" ]; then
                       cp "$file" ${upload_dir}/sigs_aa/
                    else
                       echo "No files found matching the pattern. Missing protein sketch."
                    fi
                 done
                 for file in /localdisk/${accession}/${accession}.unitigs.fa_gather_*; do
                    if [ -e "$file" ]; then
                       cp "$file" ${upload_dir}/func_profiles/
                    else
                       echo "No files found matching the pattern."
                    fi
                 done
                 s5cmd cp -c 1 ${upload_dir} s3://serratus-rayan/beetles/logan_feb12_run/yacht/$accession/
            else
                echo "One or more output files are missing for $accession"
                ls result_0.95_ANI_${accession}.xlsx ${accession}.unitigs.fa.sig.zip ${accession}_functional_profile || true
            fi

    fi

	rm -Rf /localdisk/"$accession"
	echo "Done with $accession"
	trap '' EXIT
}

export -f task
